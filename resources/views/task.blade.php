@extends('layout')
@section('content')
<div class="container">
  <div class="row text-center">
    <div class="col-md-4">
    </div>

    <div class="col-md-3 text-center">
      <a class="btn btn-primary" href="/task/create">Add Task</a>
    </div>
  </div>
  <br/>
  <div class="row">
    <table id="task" class="table table-striped table-reponsive table-bordered" width="100%" cellspacing="0">
      <thead>
        <tr>
          <td>Name</td>
          <td>Description</td>
          <td>Status</td>
          <td>Actions</td>
        </tr>
      </thead>
      <tbody>
        @foreach($tasks as $index=>$each)
          <tr>
            <td>
              {{$each->name}}
            </td>
            <td>
              {{$each->description}}
            </td>
            <td>
                @php 
                if($each->status==0) echo "TODO";
                elseif($each->status==1) echo "DONE";
                @endphp
            </td>
            <td>
                <form action="/task/{{$each->id}}" method="POST">
                @csrf
                <a class="btn btn-primary" href="/task/{{$each->id}}/edit/">Edit</a>
                @method('DELETE')
                <input class="btn btn-danger" type="submit" value="Delete" />
                </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@push('page_scripts')
<script>
$(document).ready(function() {
    $('#tasks').DataTable();
});
</script>
@endpush