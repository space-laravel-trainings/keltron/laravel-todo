@extends('layout')
@section('content')
<div class="container">
<div class="row">

<form method="POST" id="add_task" class="well form-horizontal" action="/task">
@csrf
<fieldset>

<!-- Form Name -->
<legend class="text-center"><h2><b>Add a Task</b></h2></legend><br>

<!-- Text input-->
<div class="form-group">
    <label for="name" class="col-md-4 control-label">Name</label>
    <div class="col-md-4 inputGroupContainer">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Task name" name="name"/>
        </div>
    </div>
</div>

<!-- Text description-->
<div class="form-group">
    <label for="description" class="col-md-4 control-label">Description</label>
    <div class="col-md-4 inputGroupContainer">
        <div class="input-group">
            <textarea name="description" class="form-control" rows="5"></textarea>
        </div>
    </div>
</div>

<!-- Text description-->
<div class="form-group">
    <label class="col-md-4 control-label">Status</label>
    <label for="todo" class="col-md-1 control-label">
    Todo <input id="todo" type="radio" name="status" value="0" />
    </label>
    <label for="done" class="col-md-1 control-label">
    Done <input id="done" type="radio" name="status" value="1" />
    </label>
</div>

<!-- Button -->
<div class="form-group">
<label class="col-md-4 control-label"></label>
<div class="col-md-4"><br>
<button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
</div>
</div>

<div class="col-md-4 col-md-offset-4" id="firstCol"></div>
<div id="secondCol" class="col-md-8 col-md-offset-2"></div>

</fieldset>
</form>
</div>

</div>
</div>
@endsection